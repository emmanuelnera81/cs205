﻿using UnityEngine;

public class RollyBall : MonoBehaviour {
    public float speed = 10;
    
    public Rigidbody rb;
    public Vector2 myInput;

    void Start() {
        rb = GetComponent<Rigidbody>();
    }

    private void Update() {
        myInput = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
    }

    private void FixedUpdate() {
        rb.velocity = new Vector3(myInput.x * speed, rb.velocity.y, myInput.y * speed);
    }

    private void OnTriggerEnter(Collider other) {
        Destroy(other.gameObject);
    }
}
