﻿using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour {
    private NavMeshAgent agent;
    private int hp = 10;

    void Start() {
        agent = GetComponent<NavMeshAgent>();
    }

    void Update() {
        agent.SetDestination(GameObject.Find("Player").transform.position);
    }

    private void OnCollisionEnter(Collision collision) {
        Bullet bullet = collision.transform.GetComponent<Bullet>();
        
        if (bullet) {
            bullet.Explode();
            
            hp--;

            if (hp <= 0) {
                Destroy(gameObject);
            }
        }
    }
}
