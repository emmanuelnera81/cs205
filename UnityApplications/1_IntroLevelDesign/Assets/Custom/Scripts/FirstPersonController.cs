﻿using UnityEngine;

public class FirstPersonController : MonoBehaviour {
    public float speed = 5;
    public float mouseSensitivity = 2;
    public float jumpPower = 1;

    private Vector2 inputMove;
    private Vector2 inputTurn;
    private Rigidbody rb;
    private bool isRequestJump;
    private bool isGrounded;

    void Start() {
        Cursor.lockState = CursorLockMode.Locked;
        rb = GetComponent<Rigidbody>();
    }

    void Update() {
        GetInput();
    }

    private void FixedUpdate() {
        Move();
        CheckForGround();
        Jump();
        Turn();
    }

    private void GetInput() {
        inputMove.x = Input.GetAxis("Horizontal");
        inputMove.y = Input.GetAxis("Vertical");
        
        inputTurn.x = Input.GetAxis("Mouse X");
        inputTurn.y = Input.GetAxis("Mouse Y");

        if (Input.GetButtonDown("Jump")) {
            isRequestJump = true;
        }
    }

    private void Turn() {
        transform.localEulerAngles += Vector3.up * inputTurn.x * mouseSensitivity;
        Camera.main.transform.localEulerAngles -= Vector3.right * inputTurn.y * mouseSensitivity;
    }

    private void Move() {
        Vector3 newVelocity = transform.right * speed * inputMove.x;
        newVelocity += transform.forward * speed * inputMove.y;
        newVelocity.y = rb.velocity.y;
        rb.velocity = newVelocity;
    }

    private void Jump() {
        //if (isRequestJump) {
        if (isRequestJump && isGrounded) {
            rb.velocity += Vector3.up * jumpPower;
        }

        isRequestJump = false;
    }

    private void CheckForGround() {
        RaycastHit hit;
        Vector3 origin = transform.position + (Vector3.up * 0.1f);
        float maxDistance = 0.2f;
        Physics.Raycast(origin, Vector3.down, out hit, maxDistance);
        Debug.DrawRay(origin, Vector3.down * maxDistance);

        isGrounded = false;
        if (hit.transform) {
            isGrounded = true;
        }
    }
}
