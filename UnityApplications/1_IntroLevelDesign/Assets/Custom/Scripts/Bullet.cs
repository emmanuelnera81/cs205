﻿using UnityEngine;

public class Bullet : MonoBehaviour {
    public float power;
    public ParticleSystem explosionPrefab;
    public Rigidbody rb;

    private void Start() {
        rb = GetComponent<Rigidbody>();
        rb.velocity = Camera.main.transform.forward * power;
    }

    private void OnCollisionEnter(Collision collision) {
        if (collision.transform.CompareTag("KillBox")) {
            Explode();
        }
    }

    public void Explode() { 
        Instantiate(explosionPrefab, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}
