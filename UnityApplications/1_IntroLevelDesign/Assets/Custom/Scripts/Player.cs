﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour {
    private void OnCollisionEnter(Collision collision) {
        if (collision.transform.CompareTag("KillBox") || collision.transform.CompareTag("Enemy")) {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}
