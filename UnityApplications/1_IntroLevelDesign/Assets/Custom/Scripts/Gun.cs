﻿using UnityEngine;

public class Gun : MonoBehaviour {
    public Bullet prefab;

    private void Update() {
        if (Input.GetMouseButtonDown(0)) {
            Instantiate(prefab, Camera.main.transform.position + Camera.main.transform.forward, Quaternion.identity);
        }
    }
}
