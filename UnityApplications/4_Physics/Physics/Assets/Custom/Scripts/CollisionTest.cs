﻿using UnityEngine;

public class CollisionTest : MonoBehaviour {
    private Material mat;

    private void Start() {
        mat = GetComponent<Renderer>().material;
    }

    private void OnCollisionEnter(Collision collision) {
        Destroy(collision.gameObject);
        mat.color = Color.yellow;
    }

    private void OnCollisionExit(Collision collision) {
        mat.color = Color.red;
    }
}
