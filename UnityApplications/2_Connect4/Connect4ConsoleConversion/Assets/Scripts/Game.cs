﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviour {
    public enum PlayerID {
        None
    ,   Yellow
    ,   Red
    }

    // When the game starts.
    public void Awake() {
        Instance = this;
        turn = UnityEngine.Random.Range(0, 2) == 0 ? PlayerID.Yellow : PlayerID.Red;
        Generate();
        
        StartCoroutine(ProcessAndRender());
    }
        
    public static readonly Vector2Int BoardSize = new Vector2Int(7, 6);

    public static Game Instance;

    public char[,] board = new char[BoardSize.x, BoardSize.y];
    //public ConsoleKeyInfo Info;
    public int key = 0;
    public bool IsRunning = true;

    private static int scoreYellow = 0;
    private static int scoreRed = 0;
        
    private Display display = new Display();
    private PlayerID turn;
    private PlayerID victor = PlayerID.None;

    private void Generate() {
        for (int y = 0; y < BoardSize.y; y++) {
            for (int x = 0; x < BoardSize.x; x++) {
                board[x, y] = '-';
            }
        }
    }

    private IEnumerator ProcessAndRender() {
        display.Render();

        while (IsRunning) {
            Log();

            IsRunning = !IsGameOver();
            if (!IsRunning) {
                break;
            }

            //Info = Console.ReadKey(true);
            yield return StartCoroutine(GetSelectedColumn());
            yield return null;

            Act();
            display.Render();
        }

        StartCoroutine(End());
    }

    private IEnumerator End() {
        string victoryColor = victor == PlayerID.Yellow ? "<color=yellow>" : "<color=red>";
        Debug.Log(victoryColor + victor.ToString() + "has won</color>. Press ENTER to play again.");

        while (!Input.GetKeyDown(KeyCode.Return)) {
            yield return null;
        }

        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    private IEnumerator GetSelectedColumn() {
        Debug.Log("Please select a row.");
        key = 0;

        while (key < 1) {
            if (Input.anyKeyDown && int.TryParse(Input.inputString, out int input)) {
                key = input > 0 ? input : 0;
                Debug.Log(key + " is pressed.");
                // log it.

                yield break;
            }

            yield return null;
        }
    }

    public void Act() {
        if (key > BoardSize.x) {
            return;
        }

        key--;

        if (key < 0) {
            return;
        }

        PlaceTile(key);
    }

    private void PlaceTile(int selectedColumn) {
        for (int i = 0; i < BoardSize.y; i++) {
            char c = board[selectedColumn, i];
            //Console.WriteLine(c + ", " + i);

            if (c == '-') {
                board[selectedColumn, i] = turn == PlayerID.Yellow ? 'X' : 'O';
                    
                CheckForVictory(new Vector2Int(selectedColumn, i));

                turn = turn == PlayerID.Yellow ? PlayerID.Red : PlayerID.Yellow;

                break;
            }
        }
    }

    private void CheckForVictory(Vector2Int _coord) {
        char c = board[_coord.x, _coord.y];
        int h = _coord.x;
        int v = _coord.y;

        if (CheckHorizontal(c, h, v)
        ||  CheckVertical(c, h, v)
        ||  CheckDiagonalToNorthEast(c, h, v)
        ||  CheckDiagonalToSouthEast(c, h, v)
        ) {
            victor = turn;
        }
    }

    private bool CheckHorizontal(char c, int h, int v) {
        int matchesFound = 1;
            
        // Check left.
        for (int i = 1; i < 4; i++) {
            if (h - i < 0 || board[h - i, v] != c) {
                break;
            }

            matchesFound++;
        }

        // Check right.
        for (int i = 1; i < 4; i++) {
            if (h + i > BoardSize.y - 1 || board[h + i, v] != c) {
                break;
            }

            matchesFound++;
        }

        if (matchesFound == 4) {
            //Console.WriteLine("It's a Horizontal match!");
            return true;
        }

        return false;
    }

    private bool CheckVertical(char c, int h, int v) {
        int matchesFound = 1;
            
        // Check down.
        for (int i = 1; i <= 4; i++) {
            if (v - i < 0 || board[h, v - i] != c) {
                break;
            }

            matchesFound++;
        }

        // Check up.
        for (int i = 1; i <= 4; i++) {
            if (v + i > BoardSize.y - 1 || board[h, v + i] != c) {
                break;
            }

            matchesFound++;
        }

        if (matchesFound == 4) {
            //Console.WriteLine("It's a vertical match!");
            return true;
        }

        return false;
    }

    private bool CheckDiagonalToNorthEast(char c, int h, int v) {
        int matchesFound = 1;
        Vector2Int coord = new Vector2Int(h, v);

        // Check towards SW.
        for (int i = 1; i <= 4; i++) {
            Vector2Int direction = Vector2Int.one * -i;
            Vector2Int sampleCoord = coord + direction;
            if (sampleCoord.IsLessThan(Vector2Int.zero) || board[sampleCoord.x, sampleCoord.y] != c) {
                break;
            }


            matchesFound++;
        }

        // Check towards NE.
        for (int i = 1; i <= 4; i++) {
            Vector2Int direction = Vector2Int.one * i;
            Vector2Int sampleCoord = coord + direction;
            if (sampleCoord.IsGreaterThanOrEqualTo(BoardSize) || board[sampleCoord.x, sampleCoord.y] != c) {
                break;
            }

            matchesFound++;
        }

        if (matchesFound == 4) {
            //Console.WriteLine("It's a northeast match!");
            return true;
        }

        return false;
    }

    private bool CheckDiagonalToSouthEast(char c, int h, int v) {
        int matchesFound = 1;
        Vector2Int coord = new Vector2Int(h, v);

        // Check towards SW.
        for (int i = 1; i <= 4; i++) {
            Vector2Int direction = new Vector2Int(-i, i);
            Vector2Int sampleCoord = coord + direction;
            if (sampleCoord.IsLessThan(Vector2Int.zero) || sampleCoord.IsGreaterThanOrEqualTo(BoardSize) || board[sampleCoord.x, sampleCoord.y] != c) {
                break;
            }

            matchesFound++;
        }

        // Check towards NE.
        for (int i = 1; i <= 4; i++) {
            Vector2Int direction = new Vector2Int(i, -i);
            Vector2Int sampleCoord = coord + direction;
            if (sampleCoord.IsLessThan(Vector2Int.zero) || sampleCoord.IsGreaterThanOrEqualTo(BoardSize) || board[sampleCoord.x, sampleCoord.y] != c) {
                break;
            }

            matchesFound++;
        }

        if (matchesFound == 4) {
            //Console.WriteLine("It's a southeast match!");
            return true;
        }

        return false;
    }

    private bool IsGameOver() {
        if (victor != PlayerID.None) {
            IsRunning = false;

            if (victor == PlayerID.Yellow) {
                scoreYellow++;
            } else {
                scoreRed++;
            }

            // Haxx in a render before we restart the game.
            display.Render();
            Log();

            return true;
        }
        return false;
    }

    public void Log() {
        Debug.Log($"<color=yellow>Yellow: {scoreYellow}</color>\t<color=red>Red: {scoreRed}</color>");
        Debug.Log($"Choose a column from 1 - {BoardSize.x}");

        if (victor == PlayerID.None) {
            string color = turn == PlayerID.Yellow ? "<color=yellow>" : "<color=yellow>";
            Debug.Log($"{color} {turn.ToString()}'s turn...");
        }
    }
}
