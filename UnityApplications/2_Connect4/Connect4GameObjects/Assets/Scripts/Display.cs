﻿using System;
using UnityEngine;

public class Display {
    public Display() {
        //Console.WindowHeight = Game.BoardSize.y + 10;
        //Console.WindowWidth = Game.BoardSize.x + 5;
    }

    public void Render() {
        //Console.SetCursorPosition(0, 0);

        string output = "";

        for (int y = Game.BoardSize.y - 1; y >= 0; y--) {
            output += "||\t";

            for (int x = 0; x < Game.BoardSize.x; x++) {
                output += GetObjectColor(Game.Instance.board[x, y], out Color _colour, out float _scale) + Game.Instance.board[x, y] + "</color>\t";
                Game.Instance.tiles[x, y].GetComponent<Renderer>().material.color = _colour;
                Game.Instance.tiles[x, y].localScale = Vector3.one * _scale;
            }

            output += "||\n";
        }

        RenderGuide(ref output);

        Debug.Log(output);
    }

    private static void RenderGuide(ref string _output) {
        _output += "||\t";
        for (int x = 0; x < Game.BoardSize.x; x++) {
            _output += (x + 1) + "\t";
        }
        
        _output += "||";
    }

    private string GetObjectColor(char _c, out Color _colour, out float _scale) {
        string value = "";

        switch (_c) {
            case 'O':
                value = "red";
                _colour = Color.red;
                _scale = 0.94f;
                break;
            case 'X':
                value = "yellow";
                _colour = Color.yellow;
                _scale = 0.94f;
                break;
            default:
                value = "black";
                _colour = Color.black;
                _scale = 0.3f;
                break;
        }

        return "<color=" + value + ">";
    }
}
