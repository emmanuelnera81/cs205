﻿using System;

namespace _2_Adventure {
    public class Enemy : Character {
        public Enemy(string _name, int _hp, int _mp, int _strength, int _defense, int _intelligence, int _spirit, int _accuracy, int _evasion, int _luck, int _exp, int _qtyPotions) : base(_name) {
            SetStats(_hp, _mp, _strength, _defense, _intelligence, _spirit, _accuracy, _evasion, _luck);
            exp = _exp;
            qtyPotions = _qtyPotions;
        }

        public void Attack(Character _target) {
            if (!AbilityDatabase.IsHit(this, _target)) { return; }

            int damage = CalculateAttackDamage();
            _target.hp -= damage;
            _target.hp = _target.hp < 0 ? 0 : _target.hp;

            Game.WriteStandard(name + " attacks " + _target.name + " for " + damage + "dmg.", ConsoleColor.Red, false);
        }

        protected int CalculateAttackDamage() {
            return strength / 4 + ((int)(new Random().NextDouble()) * (strength / 4) * GetCriticalMultiplier());
        }
    }
}
