﻿using System;

namespace _2_Adventure {
    public struct Ability {
        public Ability(string _name, int _mpCost, float _multiplier, Action<Character, float> _action) {
            name = _name;
            mpCost = _mpCost;
            power = _multiplier;
            action = _action;
        }

        public string name;
        public int mpCost;
        public float power;
        public Action<Character, float> action;
    }
}
