﻿using System.Collections.Generic;

namespace _2_Adventure {
    class WeaponDatabase {
        public static Dictionary<string, Weapon> Swords = new Dictionary<string, Weapon>() {
            { "Wooden Sword" , new Weapon("Wooden Sword" , Weapon.WeaponType.SWORD, 0, 8,  0, 14) }
        ,   { "Steel Blade"  , new Weapon("Steel Blade"  , Weapon.WeaponType.SWORD, 0, 20, 0, 50) }
        ,   { "Diamond Shard", new Weapon("Diamond Shard", Weapon.WeaponType.SWORD, 0, 28, 0, 70) }
        ,   { "Broken Bottle", new Weapon("Broken Bottle", Weapon.WeaponType.SWORD, 0, 2,  0, 6) }
        };

        public static Dictionary<string, Weapon> Knives = new Dictionary<string, Weapon>() {
            { "Pocket Knife"   , new Weapon("Pocket Knife"   , Weapon.WeaponType.KNIFE, 0, 3, 0, 14) }
        ,   { "Butterfly Knife", new Weapon("Butterfly Knife", Weapon.WeaponType.KNIFE, 0, 5, 0, 18) }
        ,   { "Jack Knife"     , new Weapon("Jack Knife"     , Weapon.WeaponType.KNIFE, 0, 8, 0, 24) }
        };

        public static Dictionary<string, Weapon> Staves = new Dictionary<string, Weapon>() {
            { "Stick"      , new Weapon("Stick"      , Weapon.WeaponType.STAFF, 4,  2,  20, 12) }
        ,   { "Carved Oak" , new Weapon("Carved Oak" , Weapon.WeaponType.STAFF, 16, 8,  26, 12) }
        ,   { "Steel Staff", new Weapon("Steel Staff", Weapon.WeaponType.STAFF, 32, 16, 32, 12) }
        };
    }
}
