﻿using System;

namespace _2_Adventure {
    public abstract class Character {
        public Character(string _name) {
            name = _name;
        }

        public string name;
        public int hp;
        public int maxHP;
        public int mp;
        public int maxMP;
        public int strength;
        public int defense;
        public int intelligence;
        public int spirit;
        public int accuracy;
        public int evasion;
        public int luck;
        public int exp;
        public int qtyPotions;

        protected void SetStats(int _hp, int _mp, int _strength, int _defense, int _intelligence, int _spirit, int _accuracy, int _evasion, int _luck) {
            hp = _hp;
            maxHP = hp;
            mp = _mp;
            maxMP = mp;
            strength = _strength;
            defense = _defense;
            intelligence = _intelligence;
            spirit = _spirit;
            accuracy = _accuracy;
            evasion = _evasion;
            luck = _luck;
        }

        public int GetCriticalMultiplier() {
            int criticalMultiplier = (luck > (int)(new Random().NextDouble() * 255)) ? 2 : 1;

            if (criticalMultiplier == 2) {
                Game.WriteStandard(name + " lands a critical Hit!\n", ConsoleColor.Gray, false);
            }

            return criticalMultiplier;
        }
    }
}
