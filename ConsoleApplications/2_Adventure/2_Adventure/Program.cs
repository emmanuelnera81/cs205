﻿using System;

namespace _2_Adventure {
    class Program {
        static void Main() {
            Game.Play();

            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine();
            Console.WriteLine("Thanks for playing! Press any key to close.");
            Console.ReadKey(true);
        }
    }
}
