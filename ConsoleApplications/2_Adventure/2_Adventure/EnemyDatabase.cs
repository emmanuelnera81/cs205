﻿using System.Collections.Generic;

namespace _2_Adventure {
    class EnemyDatabase {
        public static Dictionary<string, Enemy> Enemies = new Dictionary<string, Enemy>() {
            { "Bandersnatch", new Enemy("Bandersnatch", 32, 0,  24, 14, 8,  9,  17, 15, 20, 1000, 2) }
        ,   { "Goblin"      , new Enemy("Goblin"      , 18, 24, 14, 12, 18, 18, 20, 20, 14, 250, 1) }
        ,   { "Soldier"     , new Enemy("Soldier"     , 38, 12, 18, 18, 14, 14, 20, 8,  20, 400, 1) }
        };
    }
}
