﻿using System;

namespace Connect4 {
    class Game {
        public Game() {
            Instance = this;

            Generate();
            ProcessAndRender();
        }

        public enum PlayerID {
            None
        ,   Red
        ,   Yellow
        }

        public static readonly Vector2 BoardSize = new Vector2(7, 6);
        
        public static Game Instance;

        public char[,] board = new char[BoardSize.x , BoardSize.y];
        public ConsoleKeyInfo info;
        public bool isRunning = true;
        
        private static int scoreRed;
        private static int scoreYellow;

        private string log;
        private Renderer renderer = new Renderer();
        private PlayerID turn = new Random().Next(0, 2) == 0 ? PlayerID.Yellow : PlayerID.Red;
        private PlayerID victor = PlayerID.None;

        public void Act() {
            log = "";

            if (!int.TryParse(info.KeyChar.ToString(), out int selectedColumn) || selectedColumn > BoardSize.x) {
                return;
            }

            selectedColumn--;

            if (selectedColumn < 0) {
                return;
            }

            PlaceTile(selectedColumn);
        }

        private bool CheckDiagonalToNorthEast(char _c, int _h, int _v) {
            int matchesFound = 1;
            Vector2 coord = new Vector2(_h, _v);

            // Check towards SW.
            for (int i = 1; i < 4; i++) {
                Vector2 direction = Vector2.One * -i;
                Vector2 sampleCoord = coord + direction;

                if (sampleCoord < Vector2.Zero || board[sampleCoord.x, sampleCoord.y] != _c) {
                    break;
                }

                matchesFound++;
            }

            // Check towards NE.
            for (int i = 1; i < 4; i++) {
                Vector2 direction = Vector2.One * i;
                Vector2 sampleCoord = coord + direction;

                if (sampleCoord >= BoardSize || board[sampleCoord.x, sampleCoord.y] != _c) {
                    break;
                }

                matchesFound++;
            }

            if (matchesFound == 4) {
                return true;
            }

            return false;
        }

        private bool CheckDiagonalToSouthEast(char _c, int _h, int _v) {
            return false;
        }
        
        private bool CheckHorizontal(char _c, int _h, int _v) {
            int matchesFound = 1;

            // Check left.
            for (int i = 1; i < 4; i++) {
                if (_h - i < 0 || board[_h - i, _v] != _c) {
                    break;
                }

                matchesFound++;
            }

            // Check right.
            for (int i = 1; i < 4; i++) {
                if (_h + i > BoardSize.x || board[_h + i, _v] != _c) {
                    break;
                }

                matchesFound++;
            }

            if (matchesFound == 4) {
                return true;
            }

            return false;
        }

        private bool CheckVertical(char _c, int _h, int _v) {
            return false;
        }

        private void CheckForVictory(Vector2 _coord) {
            char c = board[_coord.x, _coord.y];
            int h = _coord.x;
            int v = _coord.y;

            if (CheckHorizontal(c, h, v) 
            || CheckVertical(c, h, v)
            || CheckDiagonalToNorthEast(c, h, v)
            || CheckDiagonalToSouthEast(c, h, v)
            ) {
                victor = turn;
            }
        }

        public void ClearDialogue() {
            int linesToClear = 6;
            string output = "";

            for (int i = 0; i < linesToClear; i++) {
                for (int j = 0; j < Console.WindowWidth; j++) {
                    output += " ";
                }
            }

            Console.Write(output);
        }

        private void Generate() {
            for (int y = 0; y < BoardSize.y; y++) {
                for (int x = 0; x < BoardSize.x; x++) {
                    board[x, y] = '-';
                }
            }
        }

        private bool IsGameOver() {
            if (victor != PlayerID.None) {
                isRunning = false;

                if (victor == PlayerID.Yellow) {
                    scoreYellow++;
                } else {
                    scoreRed++;
                }

                renderer.Render();
                Log();

                Console.ForegroundColor = victor == PlayerID.Yellow ? ConsoleColor.Yellow : ConsoleColor.Red;
                Console.WriteLine("\n" + victor.ToString() + " has won. Press ENTER to play again.");

                ConsoleKey key = ConsoleKey.D0;
                while (key != ConsoleKey.Enter) {
                    key = Console.ReadKey(true).Key;
                }

                return true;
            }

            return false;
        }

        public void Log() {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("Yellow: " + scoreYellow);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("\tRed: " + scoreRed);

            Console.ResetColor();
            Console.Write("\nChoose a column from 1-" + BoardSize.x);

            if (victor == PlayerID.None) {
                Console.ForegroundColor = (turn == PlayerID.Yellow) ? ConsoleColor.Yellow : ConsoleColor.Red;
                Console.Write("\n" + turn.ToString() + "'s turn...");
            }

            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.Write(log);
        }

        private void PlaceTile(int selectedColumn) {
            for (int y = 0; y < BoardSize.y; y++) {
                char c = board[selectedColumn, y];

                if (c == '-') {
                    board[selectedColumn, y] = (turn == PlayerID.Yellow) ? 'X' : 'O';

                    CheckForVictory(new Vector2(selectedColumn, y));

                    turn = (turn == PlayerID.Yellow) ? PlayerID.Red : PlayerID.Yellow;

                    break;
                }
            }
        }

        private void ProcessAndRender() {
            renderer.Render();

            while (isRunning) {
                Log();

                isRunning = !IsGameOver();
                if (!isRunning) {
                    return;
                }

                ClearDialogue();

                info = Console.ReadKey(true);

                Act();
                renderer.Render();
            }
        }
    }
}
